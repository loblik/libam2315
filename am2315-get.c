/*
 * libam2315 is simple C interface for am2315 i2c sensor
 * Copyright (C) 2014  Pavel Löbl
 *
 * libam2315 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libam2315 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libam2315.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include "am2315.h"

int main(int argc, char *argv[]) {
	struct am2315 am;
	float temp, humidity;
	if (argc != 2) {
		printf("am2315 [I2C-DEV]\n");
		exit(EXIT_FAILURE);
	}

	if (am2315_init(&am, argv[1]))
		exit(EXIT_FAILURE);

	if (am2315_read_all(&am, &temp, &humidity))
		exit(EXIT_FAILURE);

	printf("%.1f %.1f\n", temp, humidity);

	return EXIT_SUCCESS;
}
