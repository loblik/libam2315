/*
 * libam2315 is simple C interface for am2315 i2c sensor
 * Copyright (C) 2014  Pavel Löbl
 *
 * libam2315 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libam2315 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libam2315.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _BSD_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "am2315.h"

#define AM2315_ADDR			0x5c
#define AM2315_DELAY		20000

#define AM2315_QUERY_TEMP	((unsigned char[]){ 0x03, 0x02, 0x02 })
#define AM2315_QUERY_RH		((unsigned char[]){ 0x03, 0x00, 0x02 })
#define AM2315_QUERY_ALL	((unsigned char[]){ 0x03, 0x00, 0x04 })
#define AM2315_QUERY_INFO	((unsigned char[]){ 0x03, 0x08, 0x07 })

#define AM2315_QUERY_TEMP_LENGTH	3
#define AM2315_QUERY_RH_LENGTH		3
#define AM2315_QUERY_ALL_LENGTH		3
#define AM2315_QUERY_INFO_LENGTH	3

#define AM2315_RESP_RH_LOW_RH		3
#define AM2315_RESP_RH_HIGH_RH		2
#define AM2315_RESP_RH_LENGTH		6
#define AM2315_RESP_RH_CRC			4

#define AM2315_RESP_TEMP_LOW_TEMP	3
#define AM2315_RESP_TEMP_HIGH_TEMP	2
#define AM2315_RESP_TEMP_LENGTH		6
#define AM2315_RESP_TEMP_CRC		4

#define AM2315_RESP_ALL_LOW_TEMP	5
#define AM2315_RESP_ALL_HIGH_TEMP	4
#define AM2315_RESP_ALL_LOW_RH		3
#define AM2315_RESP_ALL_HIGH_RH		2
#define AM2315_RESP_ALL_CRC			6
#define AM2315_RESP_ALL_LENGTH		8

static float am2315_calculate_temp(unsigned char high, unsigned char low);
static float am2315_calculate_rh(unsigned char high, unsigned char low);

static int i2c_write(struct am2315 *sensor, const unsigned char *byte, int len);
static int i2c_read(struct am2315 *sensor, unsigned char *byte, int len);

static int am2315_wakeup(struct am2315 *sensor);
static int am2315_check_crc(const unsigned char *buffer, int len);

int am2315_init(struct am2315 *sensor, const char *device) {

	sensor->fd = open(device, O_RDWR);
	if (sensor->fd < 0)
		return AM2315_INVAL_DEVICE;

	if(ioctl(sensor->fd, I2C_SLAVE, AM2315_ADDR) < 0) {
		return AM2315_INVAL_DEVICE;
	}

	unsigned char buffer[AM2315_QUERY_INFO_LENGTH];

	am2315_wakeup(sensor);

	i2c_write(sensor, AM2315_QUERY_INFO, AM2315_QUERY_INFO_LENGTH);
	usleep(AM2315_DELAY);
	i2c_read(sensor, buffer, AM2315_QUERY_INFO_LENGTH);

	if (buffer[0] == 0x03 && buffer[1] == 0x07)
		return AM2315_SUCCESS;
	else
		return AM2315_INIT_FAILED;
}

int am2315_wakeup(struct am2315 *sensor) {
	uint8_t reg;
	write(sensor->fd, &reg, 0);
	return AM2315_SUCCESS;
}

static int i2c_write(struct am2315 *sensor, const unsigned char *byte, int len) {
	int ret;
	while ((ret = write(sensor->fd, byte, len)) < 0 && errno == EINTR);
#ifdef _DEBUG
	int i;
	for (i = 0; i < len; i++)
		printf("w: 0x%x\n", byte[i]);
#endif
	if (ret < len) {
		perror("i2c_write");
	}
	return ret < 0;
}

static int i2c_read(struct am2315 *sensor, unsigned char *byte, int len) {
	int ret;
	while ((ret = read(sensor->fd, byte, len)) < 0 && errno == EINTR);
#ifdef _DEBUG
	int i;
	for (i = 0; i < len; i++)
		printf("r: 0x%x\n", byte[i]);
#endif
	if (ret < len) {
		perror("i2c_write");
	}
	return ret < 0;
}

static float am2315_calculate_temp(unsigned char high, unsigned char low) {
	float temp;
	int sign = high << 1;

	high = (high << 1) >> 1;
	/* we can ommit highest bits since 405 degrees of celsia
	   is more than enouhg for the sensor */
	temp = (high & 0x0f) * 256 + ((low & 0xf0) >> 4) * 16 + (low & 0x0f);
	temp *= 0.1;
	if (sign)
		temp *= -1;
	return temp;
}

static float am2315_calculate_rh(unsigned char high, unsigned char low) {
	float rh;

	rh = (high & 0x0f) * 256 + ((low & 0xf0) >> 4) * 16 + (low & 0x0f);
	rh *= 0.1;

	return rh;
}

int am2315_read_temp(struct am2315 *sensor, float *temp) {
	unsigned char buffer[AM2315_RESP_TEMP_LENGTH];
	am2315_wakeup(sensor);
	if (i2c_write(sensor, AM2315_QUERY_TEMP, AM2315_QUERY_TEMP_LENGTH))
		return AM2315_IO_ERROR;
	usleep(AM2315_DELAY);
	if (i2c_read(sensor, buffer, AM2315_RESP_TEMP_LENGTH))
		return AM2315_IO_ERROR;

	if (am2315_check_crc(buffer, AM2315_RESP_TEMP_LENGTH))
		return AM2315_CRC_FAILED;

	unsigned char high = buffer[AM2315_RESP_TEMP_HIGH_TEMP];
	unsigned char low = buffer[AM2315_RESP_TEMP_LOW_TEMP];

	*temp = am2315_calculate_temp(high, low);

	return AM2315_SUCCESS;
}

int am2315_read_humidity(struct am2315 *sensor, float *humidity) {
	unsigned char buffer[AM2315_RESP_RH_LENGTH];
	am2315_wakeup(sensor);

	if (i2c_write(sensor, AM2315_QUERY_RH, AM2315_QUERY_RH_LENGTH))
		return AM2315_IO_ERROR;
	usleep(AM2315_DELAY);
	if (i2c_read(sensor, buffer, AM2315_RESP_RH_LENGTH))
		return AM2315_IO_ERROR;

	if (am2315_check_crc(buffer, AM2315_RESP_RH_LENGTH))
		return AM2315_CRC_FAILED;

	unsigned char high = buffer[AM2315_RESP_RH_HIGH_RH];
	unsigned char low = buffer[AM2315_RESP_RH_LOW_RH];
	/* we can ommit highest bits since 405 degrees of celsia
	   is more than enough for the sensor */
	*humidity = am2315_calculate_rh(high, low);

	return AM2315_SUCCESS;
}

int am2315_read_all(struct am2315 *sensor, float *temp, float *humidity) {
	unsigned char buffer[AM2315_RESP_ALL_LENGTH];
	am2315_wakeup(sensor);

	if (i2c_write(sensor, AM2315_QUERY_ALL, AM2315_QUERY_ALL_LENGTH))
		return AM2315_IO_ERROR;
	usleep(AM2315_DELAY);
	if (i2c_read(sensor, buffer, AM2315_RESP_ALL_LENGTH))
		return AM2315_IO_ERROR;

	if (am2315_check_crc(buffer, AM2315_RESP_ALL_LENGTH))
		return AM2315_CRC_FAILED;

	unsigned char temp_high = buffer[AM2315_RESP_ALL_HIGH_TEMP];
	unsigned char temp_low = buffer[AM2315_RESP_ALL_LOW_TEMP];
	unsigned char rh_high = buffer[AM2315_RESP_ALL_HIGH_RH];
	unsigned char rh_low = buffer[AM2315_RESP_ALL_LOW_RH];

	*temp = am2315_calculate_temp(temp_high, temp_low);
	*humidity = am2315_calculate_rh(rh_high, rh_low);

	return AM2315_SUCCESS;
}

uint16_t crc16(const unsigned char *ptr, int len) {
	uint16_t crc = 0xffff;
	int i;
	while(len--) {
		crc ^= *ptr++;
		for(i = 0; i < 8; i++) {
			if(crc & 0x1) {
				crc >>= 1;
				crc ^= 0xa001;
			} else {
				crc >>= 1;
			}
		}
	}
	return crc;
}

int am2315_check_crc(const unsigned char *frame, int len) {
	uint16_t crc = crc16(frame, len - 2);
	return ((((crc & 0xff00) >> 8) == frame[len - 1]) &&
			((crc & 0xff) == frame[len - 2])) == 0;
}
