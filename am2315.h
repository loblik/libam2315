/*
 * libam2315 is simple C interface for am2315 i2c sensor
 * Copyright (C) 2014  Pavel Löbl
 *
 * libam2315 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libam2315 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libam2315.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AM2315_H
#define AM2315_H

struct am2315 {
	int fd;
};

#define AM2315_SUCCESS			 	0
#define AM2315_IO_ERROR				-1
#define AM2315_INVAL_DEVICE			-2
#define AM2315_CRC_FAILED			-3
#define AM2315_INIT_FAILED			-4

int am2315_init(struct am2315 *sensor, const char *device);
int am2315_read_temp(struct am2315 *sensor, float *temp);
int am2315_read_humidity(struct am2315 *sensor, float *humidity);
int am2315_read_all(struct am2315 *sensor, float *temp, float *humidity);

#endif
