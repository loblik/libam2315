CC=gcc
CFLAGS=-Wall
LDFLAGS=-L .
LDLIBS=

PREFIX = $(DESTDIR)/usr/local
BINDIR = $(PREFIX)/bin
LIBDIR = $(PREFIX)/lib
INCLUDEDIR = $(PREFIX)/include

am2315:
	$(CC) $(CFLAGS) -fPIC -o libam2315.so am2315.c -shared
	$(CC) $(CFLAGS) -c am2315.c
	ar -rcs am2315.a am2315.o

	$(CC) $(CFLAGS) $(LDFLAGS) am2315-get.c -lam2315 -o am2315-get

clean:
	rm -rf am2315-get am2315-get.o am2315 libam2315.so am2315.a am2315.o 2>/dev/null

install:
	install -D am2315-get $(DESTDIR)$(BINDIR)/am2315-get
	install -D libam2315.so $(DESTDIR)$(LIBDIR)/libam2315.so
	install -D am2315.h $(DESTDIR)$(INCLUDEDIR)/am2315.h
	ldconfig $(DESTDIR)$(LIBDIR)

uninstall:
	-rm $(DESTDIR)$(BINDIR)/am2315-get
	-rm $(DESTDIR)$(LIBDIR)/libam2315.so
	-rm $(DESTDIR)$(INCLUDEDIR)/am2315.h
